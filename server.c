#include "cs510.h"
#include "request.h"

#include "ringbuffer.h"

//
// server.c: A very, very simple web server
//
// To run:
//  server <portnum (above 2000)>
//
// Repeatedly handles HTTP requests sent to this port number.
// Most of the work is done within routines written in request.c
//

// CS510: Parse the new arguments too
void getargs(int *port, int *nThreads, int *nBuffers, int argc, char *argv[])
{
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <port> <threads> <buffers>\n", argv[0]);
        exit(1);
    }
    *port = atoi(argv[1]);
    *nThreads = atoi(argv[2]);
    if (*nThreads < 1) {
        fprintf(stderr, "Error: threads must be >= 1\n");
        exit(1);
    }
    *nBuffers = atoi(argv[3]);
    if (*nBuffers < 1) {
        fprintf(stderr, "Error: buffers must be >= 1\n");
        exit(1);
    }
}

void worker(buffer* b) {
    while(1) {
        int connfd = get(b);
        requestHandle(connfd);
        Close(connfd);
    }
}

int main(int argc, char *argv[])
{
    int listenfd, connfd, port, nThreads, bufferSize, clientlen;
    struct sockaddr_in clientaddr;

    getargs(&port, &nThreads, &bufferSize, argc, argv);

    buffer b;
    make(&b, bufferSize);

    //
    // CS510: Create some threads...
    //
    pthread_t pool[nThreads];
    int i;
    for (i=0; i < nThreads; i++){
        pthread_create(&pool[i], NULL, (void*)(&worker), &b);
    }

    listenfd = Open_listenfd(port);
    while (1) {
        clientlen = sizeof(clientaddr);
        connfd = Accept(listenfd, (SA *)&clientaddr, (socklen_t *) &clientlen);
        //
        // CS510: In general, don't handle the request in the main thread.
        // Save the relevant info in a buffer and have one of the worker threads
        // do the work. However, for SFF, you may have to do a little work
        // here (e.g., a stat() on the filename) ...
        //
        put(&b, connfd);
    }

    destroy(&b);

    return 0;
}
