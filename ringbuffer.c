#include "ringbuffer.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void make(buffer* b, int size) {
    pthread_mutex_init(&b->mutex, NULL);
    pthread_cond_init(&b->notEmpty, NULL);
    pthread_cond_init(&b->notFull, NULL);

    pthread_mutex_lock(&b->mutex);
    b->buff = malloc(size * sizeof(int));
    b->size = size;
    b->count = 0;
    b->start = 0;
    b->end = 0;
    pthread_mutex_unlock(&b->mutex);
}

void destroy(buffer* b) {
    pthread_mutex_lock(&b->mutex);
    if (b) {
        free(b->buff);
        b->size = 0;
        b->count = 0;
        b->start = 0;
        b->end = 0;
    }
    pthread_mutex_unlock(&b->mutex);
    pthread_mutex_destroy(&b->mutex);
    pthread_cond_destroy(&b->notEmpty);
    pthread_cond_destroy(&b->notFull);
}

void put(buffer* b, int value) {
    pthread_mutex_lock(&b->mutex);
    while (b->count == b->size) {
        pthread_cond_wait(&b->notFull, &b->mutex);
    }
    b->buff[b->end] = value;
    b->count++;
    b->end = (b->end + 1) % b->size;
    pthread_cond_signal(&b->notEmpty);
    pthread_mutex_unlock(&b->mutex);
}

int get(buffer* b) {
    int value = -1;
    pthread_mutex_lock(&b->mutex);
    while (b->count == 0) {
        pthread_cond_wait(&b->notEmpty, &b->mutex);
    }
    value = b->buff[b->start];
    b->count--;
    b->start = (b->start + 1) % b->size;
    pthread_cond_signal(&b->notFull);
    pthread_mutex_unlock(&b->mutex);
    return value;
}


int singleThreadTest() {
    buffer b;
    make(&b, 3);

    put(&b, 1);
    put(&b, 2);
    put(&b, 3);

    assert(get(&b) == 1);
    assert(get(&b) == 2);

    put(&b, 4);

    assert(get(&b) == 3);
    assert(get(&b) == 4);

    destroy(&b);

    printf("Single-thread passed.\n");

    return 0;
}

void workerConsume(buffer* b) {
    while (1) {
        printf("Getting...\n");
        int value = get(b);
        printf("Got %d\n", value);
    }
}

void workerProduce(buffer* b) {
    int i;
        for (i=0; i<10; i++) {
        printf("Putting %d...\n", i);
        put(b, i);
        printf("Put %d\n", i);
    }
}

int multiThreadTest() {
    buffer b;
    make(&b, 2);

    pthread_t producer;
    pthread_create(&producer, NULL, (void*)(&workerProduce), &b);

    pthread_t consumers[3];

    int i;
    for (i=0; i<3; i++){
        pthread_create(&consumers[i], NULL, (void*)(&workerConsume), &b);
    }

    while(1) {
        // Verify behavior by inspection
    }

    // pthread_join(producer, NULL);

    // for (int i=0; i<3; i++) {
    //  int rv = pthread_cancel(consumers[i]);
    //  if (rv) {
    //      printf("%d", rv);
    //  }
    // }

    // printf("Multi-thread passed.\n");

    return 0;
}
