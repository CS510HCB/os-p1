#
# To compile, type "make" or make "all"
# To remove files, type "make clean"
#
OBJS = server.o request.o cs510.o client.o ringbuffer.o
TARGET = server

CC = gcc
CFLAGS = -g -Wall

LIBS = -lpthread

.SUFFIXES: .c .o

all: server client bufftest output.cgi

server: server.o request.o cs510.o ringbuffer.o
	$(CC) $(CFLAGS) -o server $^ $(LIBS)

client: client.o cs510.o
	$(CC) $(CFLAGS) -o client $^

bufftest: bufftest.o ringbuffer.o
	$(CC) $(CFLAGS) -o bufftest $^ $(LIBS)

output.cgi: output.c
	$(CC) $(CFLAGS) -o output.cgi output.c

.c.o:
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	-rm -f $(OBJS) server client bufftest output.cgi
