#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <pthread.h>

struct ringbuffer
{
    int size;
    int* buff;

    int start;
    int end;
    int count;

    pthread_mutex_t mutex;
    pthread_cond_t notEmpty;
    pthread_cond_t notFull;
};

typedef struct ringbuffer buffer;

void make(buffer* b, int size);
void destroy(buffer* b);
void put(buffer* b, int value);
int  get(buffer* b);

int singleThreadTest();
int multiThreadTest();

#endif // RINGBUFFER_H